/* eslint-disable import/no-anonymous-default-export */
import React, { useState, useEffect } from 'react';
import axios from 'axios';

export default ({ comments }) => {
    const renderedComments = comments.map(comment => {
        let content;

        if(comment.status === 'approved') {
            content = comment.content;
        }
        else if(comment.status === 'pending') {
            content = 'This comment is awaiting moderation';
        }
        else if(comment.status === 'rejected') {
            content = 'This comment has been rejected';
        }

        return <li>{comment.content}</li>;
    });

    return <ul>
        {renderedComments}
    </ul>;
}
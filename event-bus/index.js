const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');

const app = express();
app.use(bodyParser.json());

const events = [];

app.post('/events', (req, res) => {
    const event = req.body;
    
    events.push(event);

    try{
        axios.post('http://localhost:4000/events', event);
        console.log('Posted to http://localhost:4000/events')
    } catch(e) {
        console.log('Event bus post call failed to http://localhost:4000/events');
        console.log('Reason: ', e);
    }
    try {
        axios.post('http://localhost:4001/events', event);
        console.log('Posted to http://localhost:4001/events')
    } catch(e) {
        console.log('Event bus post call failed to http://localhost:4001/events');
        console.log('Reason: ', e);
    }
    try{
        axios.post('http://localhost:4002/events', event);
        console.log('Posted to http://localhost:4002/events')
    } catch(e) {
        console.log('Event bus post call failed to http://localhost:4002/events');
        console.log('Reason: ', e);
    }
    try{
        axios.post('http://localhost:4003/events', event);
        console.log('Posted to http://localhost:4003/events')
    } catch(e) {
        console.log('Event bus post call failed to http://localhost:4003/events');
        console.log('Reason: ', e);
    }


    res.send({ status: 'Ok' });
});

// Exteremely simplified and non-production grade implementation of retrieving all events that were not sent out.
app.get('/events', (req, res) => {
    res.send(events);
});

app.listen(4005, () => {
    console.log('Listening on port 4005');
});
A simple microservice app with an in-memory object model to represent a database.

This collection of services was created with node.js and the frontend is done with the React.js library.

To run, make sure node.js is installed on your local machine.
Once that's installed, run "npm install" in each of the respective directories in order to install the necessary dependencies.
Once that's done, run "npm start" in each of the respective directories to start up each service.
Once the services are running, navigate to http://localhost:3000 in order to launch the frontend.
